var cmd = document.getElementById("cmd");
var cur = document.getElementById("cursor");
var ps1 = document.getElementById("PS1");

window.addEventListener('keypress', function(e) {
	if (e.keyCode == 32) {
		cmd.innerHTML += "&nbsp;";
	} else if (e.keyCode == 13) {
		// enter key
	} else {
		cmd.innerHTML += String.fromCharCode(e.keyCode);
	}
});

window.addEventListener('keydown', function(e) {
	if (e.keyCode === 8) {
		if (cmd.innerHTML.endsWith("&nbsp;")) {
			cmd.innerHTML = cmd.innerHTML.slice(0,-6);
		} else if (cmd.innerHTML.endsWith("&amp;")) {
			cmd.innerHTML = cmd.innerHTML.slice(0,-5);
		} else {
			cmd.innerHTML = cmd.innerHTML.slice(0,-1);
		}
	} else if (e.keyCode === 13) {
		enter();
	}
});

function enter() {
	var p = document.createElement("div"),
		c = document.createElement("div"),
		f = document.createElement("div");
	p.innerHTML = ps1.innerHTML+" ";
	c.innerHTML = cmd.innerHTML;
	f.appendChild(p);
	f.appendChild(c);

	cmd.innerHTML = "";

	past.appendChild(f);
}
